#!/bin/bash
set -eu

fn="sid-minimal-live"

##
## Ensure the working directory contains me.
##

me=$(readlink -e "$0")
wd=$(dirname "${me}")
wb=$(basename "${me}")
cd "${wd}"

##
## Check that I'm running as root
##

if [ $(id -u) -ne 0 ]; then

	echo
	echo "${wb} must be run as root (or with sudo)."
	echo
	exit 1
fi

##
## Clean up ISOs from previous build run.
##

rm -f *.iso

##
## Run the debian live build process.
##

set -x

lb clean
lb config
lb build

set +x

##
## Put a version (timestamp) in the generated ISO's filename.
##

org_iso="live-image-amd64.hybrid.iso"
version="$(date +%Y%m%d.%H%M)"
my_iso="${fn}-${version}.iso"

if [ -e "${org_iso}" ]; then
	mv "$org_iso" "$my_iso"
	ln -s "$my_iso" "$org_iso"
	echo
	echo "Generated '$my_iso'"
	echo
	exit 0
else
	echo
	echo "Failed to create the Debian Live ISO."
	echo
	exit 1
fi

exit 0

